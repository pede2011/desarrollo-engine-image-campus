#pragma once
#include "Material.h"
#include "Textura.h"

class ENGINE_API MaterialPrueba2 :public Material
{
public:
	MaterialPrueba2(Textura* _text1, Textura* _text2);
	~MaterialPrueba2();
	Textura* text1;
	Textura* text2;
	void Set() override;
};
