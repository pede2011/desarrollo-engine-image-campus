#include "stdafx.h"
#include "Sprite.h"
#include "NAPGame.h"


Sprite::Sprite(Textura* _textSprite)
{
	textSprite = _textSprite;
}


Sprite::~Sprite()
{
}

void Sprite::Set()
{
	NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	//asigno tama�o sprite	
	NAPGame::gameInstance->d3ddev->SetTexture(0, textSprite->tex);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);
	//Limpio los demas canales
	NAPGame::gameInstance->d3ddev->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(2, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(3, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(4, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(5, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(6, D3DTSS_COLOROP, D3DTOP_DISABLE);
}