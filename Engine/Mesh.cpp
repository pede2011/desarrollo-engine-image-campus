#pragma once
#include "stdafx.h"
#include "Mesh.h" 
#include "NAPGame.h" //Para crear el buffer necesario
#include "GameObject.h"//Porque C++

Mesh::Mesh(Vertex vertexes[], WORD indexes[], int p_Vertex_Count, int p_Index_Count)
{
	Vertex_Count = p_Vertex_Count;
	Index_Count = p_Index_Count;

	NAPGame::gameInstance->d3ddev->CreateVertexBuffer(//Creamos el VertexBuffer
		Vertex_Count * sizeof(Vertex),//EL tama�o es el tama�o de un vertice por su cantidad
		0,//Para handles especiales. Dejar en 0
		CUSTOMFVF3DDIFF,
		D3DPOOL_MANAGED,//Indica que debe usar la VRAM
		&vb,//Puntero a interfaz de DX de VertexBuffer
		NULL);//DEJAR EN NULL

	NAPGame::gameInstance->d3ddev->CreateIndexBuffer(//Creamos el IndexBuffer. El proceso es muy similar al de VertexBuffer
		Index_Count * sizeof(WORD),
		0,
		D3DFMT_INDEX16,//Se usa INDEX16 en lugar de INDEX32 porque es mas rapido y no necesitamos tantos indices
		D3DPOOL_MANAGED,
		&ib,
		NULL);

	VOID* lockedData = NULL;//Puntero void que indica cuanta memoria se lockea

	//Memory Handler VertexBuffer
	vb->Lock(0, 0, (void**)&lockedData, 0);//De que punto a que punto del buffer se lockea memoria. Como queremos todo va de 0 a 0
	memcpy(lockedData, vertexes, Vertex_Count * sizeof(Vertex));//Cargamos los vertices al VertexBuffer
	vb->Unlock();//Liberamos memoria

	//Memory Handler IndexBuffer
	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes, Index_Count * sizeof(WORD));//Se cargan los indices al IndexBuffer
	ib->Unlock();

}

Mesh::Mesh(ColorVertex vertexes[], WORD indexes[], int p_Vertex_Count, int p_Index_Count)
{
	Vertex_Count = p_Vertex_Count;
	Index_Count = p_Index_Count;

	NAPGame::gameInstance->d3ddev->CreateVertexBuffer(//Creamos el VertexBuffer
		Vertex_Count * sizeof(ColorVertex),//EL tama�o es el tama�o de un vertice por su cantidad
		0,//Para handles especiales. Dejar en 0
		CUSTOMFVF3DDIFF,
		D3DPOOL_MANAGED,//Indica que debe usar la VRAM
		&vb,//Puntero a interfaz de DX de VertexBuffer
		NULL);//DEJAR EN NULL

	NAPGame::gameInstance->d3ddev->CreateIndexBuffer(//Creamos el IndexBuffer. El proceso es muy similar al de VertexBuffer
		Index_Count * sizeof(WORD),
		0,
		D3DFMT_INDEX16,//Se usa INDEX16 en lugar de INDEX32 porque es mas rapido y no necesitamos tantos indices
		D3DPOOL_MANAGED,
		&ib,
		NULL);

	VOID* lockedData = NULL;//Puntero void que indica cuanta memoria se lockea

							//Memory Handler VertexBuffer
	vb->Lock(0, 0, (void**)&lockedData, 0);//De que punto a que punto del buffer se lockea memoria. Como queremos todo va de 0 a 0
	memcpy(lockedData, vertexes, Vertex_Count * sizeof(ColorVertex));//Cargamos los vertices al VertexBuffer
	vb->Unlock();//Liberamos memoria

				 //Memory Handler IndexBuffer
	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes, Index_Count * sizeof(WORD));//Se cargan los indices al IndexBuffer
	ib->Unlock();

}


Mesh::~Mesh()
{
}