#include "stdafx.h"
#include "MaterialAI.h"
#include "NAPGame.h"


MaterialAI::MaterialAI(Textura* _text1)
{
	this->text1 = _text1;
}


MaterialAI::~MaterialAI()
{
}

void MaterialAI::Set()
{
	//NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

	//set texture 1
	NAPGame::gameInstance->d3ddev->SetTexture(0, text1->tex);
	/*
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSU, text1->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSV, text1->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, text1->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, text1->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, text1->texFilter);
	// 
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);*/
	
	for (int i = 1; i < 7; i++)
		NAPGame::gameInstance->d3ddev->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
}