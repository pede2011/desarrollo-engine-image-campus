#pragma once
#include "stdafx.h"
#include "Actor3D.h"
#include "NAPGame.h"

Actor3D::Actor3D() : Actor()
{
	Vertex vertexes[] =//Seteo los vertices del cuadrado y sus colores
	{
		/*
		Los vertices del cuadrado se crean en este orden:
		.------.
		|1    2|
		|3    4|
		�------�
		*/
		{ 0.0f, 0.0f  , 0.0f, D3DCOLOR_XRGB(255, 0, 0) },//Superior Izquierda, ROJO
		{ 1.0f, 0.0f  , 0.0f, D3DCOLOR_XRGB(0, 255, 0) },//Superior Derecha, VERDE
		{ 1.0f, 1.0f, 0.0f, D3DCOLOR_XRGB(0, 0, 255) },//Inferior Izquierda, AZUL
		{ 0.0f, 1.0f, 0.0f, D3DCOLOR_XRGB(255, 255, 0) }//Inferior Izquierda, AMARILLO
	};

	WORD indexes[] = { 3,0,1,3,1,2 };

	int Vertex_Count = 4;
	int Index_Count = 6;

	//propMesh = new Mesh(vertexes, indexes, Vertex_Count, Index_Count);
	transform = new Transformacion(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));

}


Actor3D::~Actor3D()
{
}

void Actor3D::Draw()
{

	/*
	//Inicio render
	NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF2D);//Selecciona que formato estamos usando, si 2D o 3D
	NAPGame::gameInstance->d3ddev->SetIndices(propMesh->ib);//Seteamos los indices

	NAPGame::gameInstance->d3ddev->SetStreamSource(0, propMesh->vb, 0, sizeof(Vertex));//Selecciona que VertexBuffer va a mostrar

	NAPGame::gameInstance->d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, propMesh->Vertex_Count, 0, propMesh->Index_Count / 3);//Copia el VertexBuffer a la memoria. Me recomendaron que divida el Index buffer por 3 , asi que ahi esta
	*/
}