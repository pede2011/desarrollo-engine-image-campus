#pragma once
#include "d3dx9math.h"
#include "d3dx9.h"
#include <vector>
#include "Actor.h"
#include "MeshRenderer.h"
#include "Transformacion.h"
#include "Animator.h"
#include"Input.h"
#include "NAPGame.h"
#include "Mesh.h"
#include "Frustum.h"
#include "ActorCamara.h"

//ASSIMP
#include "../assimp/include/Importer.hpp"
#include "../assimp/include/scene.h"
#include "../assimp/include/postprocess.h"
#include "../assimp/include/matrix4x4.h"

#pragma comment (lib, "../assimp/lib/assimp.lib")//Importo libreria ASSIMP

class ENGINE_API Test : public Actor//Heredo de Actor, porque ahi tengo el draw
{
public:
	Test(Mesh* mesh, Material* material);
	Test();	
	~Test();

	MeshRenderer* myMeshRenderer;//Componentes de MeshRenderer, para usar en Test
	Transformacion* myTransformacion;//
	Animator myAnimator;
	Mesh *mesh;
	Textura *texture;
	Material *material;
	Frustum* frustum;
	void Draw() override;//Con esto dibujo lo que saco del MeshRenderer
	void Update() override;

	std::string name;
	std::string meshName;

	Test* father;
	std::vector<Test*> sons;
	void SetParent(Test* _father);
	void AddChild(Test* child);
	void SetMesh(Mesh* m, Material* mat);
	void SetTransform(aiMatrix4x4 m);
	void SetFrustum(Frustum* _frustum);
	bool active = false;
};