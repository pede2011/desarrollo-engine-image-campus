#include "stdafx.h"
#include "TilemapRenderer.h"


TilemapRenderer::TilemapRenderer(Tilemap* _myTilemap)
{
	myTilemap = _myTilemap;
}


TilemapRenderer::~TilemapRenderer()
{
}

void TilemapRenderer::DrawTilemap() {

	NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF3DDIFF);

	NAPGame::gameInstance->d3ddev->SetTexture(0, myTilemap->textTileMap->tex);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSU, myTilemap->textTileMap->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSV, myTilemap->textTileMap->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, myTilemap->textTileMap->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, myTilemap->textTileMap->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, myTilemap->textTileMap->texFilter);

	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);

	NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	
	for (int capa = 0; capa < myTilemap->capa.size(); capa++)
		for (int fila = 0; fila < myTilemap->capa[capa].size(); fila++)
			for (int columna = 0; columna < myTilemap->capa[capa][fila].size(); columna++)
			{
				int tileIndex = myTilemap->capa[capa][fila][columna];
				//myMeshRenderer->LoadTilemap(myTilemap->mesh[tileIndex], columna, fila, 0);
				D3DXMATRIX matTrans;
				D3DXMATRIX matSca;
				D3DXMATRIX matRotX;
				D3DXMATRIX matRotY;
				D3DXMATRIX matRotZ;

				D3DXMatrixTranslation(&matTrans, fila, columna, 5);
				D3DXMatrixScaling(&matSca,myTilemap->anchoTileMap, myTilemap->altoTileMap, 5);
				D3DXMatrixRotationX(&matRotX, D3DXToRadian(0));
				D3DXMatrixRotationY(&matRotY, D3DXToRadian(0));
				D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(0));
				D3DXMATRIX mat = matRotX * matRotY * matRotZ * matSca * matTrans;
				NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_WORLD, &mat);
				//Inicio render
				NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF3DDIFF);//Selecciona que formato estamos usando, si 2D o 3D
				NAPGame::gameInstance->d3ddev->SetIndices(myTilemap->mesh[tileIndex]->ib);//Seteamos los indices
				NAPGame::gameInstance->d3ddev->SetStreamSource(0, myTilemap->mesh[tileIndex]->vb, 0, sizeof(Vertex));//Selecciona que VertexBuffer va a mostrar
				NAPGame::gameInstance->d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, myTilemap->mesh[tileIndex]->Vertex_Count, 0, myTilemap->mesh[tileIndex]->Index_Count / 3);//Copia el VertexBuffer a la memoria. Me recomendaron que divida el Index buffer por 3 , asi que ahi esta
			}
	for (int i = 1; i < 7; i++)
		NAPGame::gameInstance->d3ddev->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_DISABLE);
}