#pragma once
#include "stdafx.h"
#include "NAPGame.h"
#include "Camara.h"
#include "Sprite.h"
#include "Input.h"
#include "MeshRenderer.h"
#include <chrono>
#include <iostream>
#include <sstream>

using namespace std::chrono;

NAPGame* NAPGame::gameInstance=NULL;

NAPGame::NAPGame(HINSTANCE hInstance, int nCmdShow, int width, int height) : _hInst(hInstance), _nCmdShow(nCmdShow), myWidth(width), myHeight(height), d3ddev(NULL), d3d(NULL)
{
	gameInstance = this;//Indico que esta es la ventana del juego

	//Creamos la clase de la ventana
	WNDCLASSEX wcex;

	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana


	//Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	HWND hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"NAPGame", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		myWidth, //Ancho
		myHeight, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

	this->hWnd = &hWnd;


	//Arranco el sist de input
	Input::Initialize(hInstance, hWnd);

	//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);//Apunta y crea interfaz DX. D3D_SDK_VERSION hace que funcione en legado, asi no crashea

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;//Struct para informacion del device DX
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));//Limpia el struct
	d3dpp.Windowed = true;//Modo ventana, no fullscreen
	d3dpp.SwapEffect = D3DSWAPEFFECT_COPY;//Descarta frames viejos (evita efecto cascada)
	d3dpp.hDeviceWindow = hWnd;//Setea que ventana va a usar DX
	d3dpp.BackBufferWidth = 800;//Resolucion en X de ventana
	d3dpp.BackBufferHeight = 600;//Resolucion en Y de ventana
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


	//Creo la interfaz con la placa de video
	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de video. En DEFAULT, DX elige sola
		D3DDEVTYPE_HAL, //Render por hardware, y si no, por soft (raro)
		hWnd, //Ventana handler
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&d3ddev); //El device que se crea
	
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	d3ddev->SetRenderState(D3DRS_LIGHTING, FALSE);
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);
	
	d3ddev->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	//d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

}

NAPGame::~NAPGame()
{
}

LRESULT CALLBACK NAPGame::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hwnd, message, wParam, lParam);
}


void NAPGame::Run()
{
	long prevTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();


	while (true)//Loop eterno hasta que se cierra
	{
		currentTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		deltaTime = (currentTime - prevTime) / 1000.0f;
		prevTime = currentTime;

		MSG msg;

		MeshRenderer::meshCount = 0;

		//Saco un mensaje de la cola de mensajes si es que hay
		//sino continuo
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)//Si se apreta el boton "Cerrar", rompe el loop
		{
			break;
		}

		Update();//Updateo por si hubo transformaciones

		Input::Update();

		d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 40, 100, 100), 1.0f, 0);//Limpia la ventana y le setea un color de fondo segun valores ARGB
		d3ddev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(0, 40, 100, 100), 1.0f, 0);
		d3ddev->BeginScene();//Empieza la escena

		for (int i = 0; i < Camara::camara.size(); i++)
		{
			Camara::camara[i]->UpdateCamara();
			Draw();
		}

		static std::stringstream st;

		st.str("");
		st << MeshRenderer::meshCount;

		std::string _texture = st.str();


		int len;
		int slength = (int)_texture.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, _texture.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, _texture.c_str(), slength, buf, len);
		std::wstring r(buf);

		LPCWSTR result = r.c_str();
		SetWindowText(*this->hWnd, result);

		d3ddev->EndScene();//Termino escena
		d3ddev->Present(NULL, NULL, NULL, NULL);//Muestro escena
	}
	
}

void NAPGame::Draw()
{
	//for (int i = 0; i < Camara::camara.size(); i++)
	//{
		//Camara::camara[i]->UpdateCamara();//Voy pasando de a 1 las camaras a la escena
		for (int i = 0; i < vectorActor.size(); i++)
		{
			vectorActor[i]->Draw();//Voy pasando de a 1 los actores a la escena
		}
	//}
}

void NAPGame::Update()
{
	for (int i = 0; i < vectorActor.size(); i++)
	{
		vectorActor[i]->Update();//Mismo que Draw
	}
}