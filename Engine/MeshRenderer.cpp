#include "stdafx.h"
#include "MeshRenderer.h"
#include "NAPGame.h"
#include "Transformacion.h"

Frustum* MeshRenderer::propFrustum = NULL;
int MeshRenderer::meshCount = 0;


//Agregar conexion con transformacion
MeshRenderer::MeshRenderer(Transformacion* transformacion, Mesh* mesh, Material* material)
{
	propMesh = mesh;
	propTransform = transformacion;
	propMaterial = material;

	if (propFrustum == NULL)
		propFrustum = new Frustum();
}


MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::LoadAnimMesh(Mesh* mesh)
{
	propMesh = mesh;
}

void MeshRenderer::DrawMesh()
{	
	if (propFrustum==NULL)
	{
		NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_WORLD, &propTransform->GetMatrix());

		propMaterial->Set();

		//Inicio render
		//NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF3DDIFF);//Selecciona que formato estamos usando, si 2D o 3D
		NAPGame::gameInstance->d3ddev->SetIndices(propMesh->ib);//Seteamos los indices
		NAPGame::gameInstance->d3ddev->SetStreamSource(0, propMesh->vb, 0, sizeof(Vertex));//Selecciona que VertexBuffer va a mostrar

		NAPGame::gameInstance->d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, propMesh->Vertex_Count, 0, propMesh->Index_Count / 3);//Copia el VertexBuffer a la memoria. Me recomendaron que divida el Index buffer por 3 , asi que ahi esta
	}

	else
	{
		if (propFrustum->CheckFrustum(propMesh)==true)
		{
			NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_WORLD, &propTransform->GetMatrix());

			propMaterial->Set();

			//Inicio render
			//NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF3DDIFF);//Selecciona que formato estamos usando, si 2D o 3D
			NAPGame::gameInstance->d3ddev->SetIndices(propMesh->ib);//Seteamos los indices
			NAPGame::gameInstance->d3ddev->SetStreamSource(0, propMesh->vb, 0, sizeof(Vertex));//Selecciona que VertexBuffer va a mostrar

			NAPGame::gameInstance->d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, propMesh->Vertex_Count, 0, propMesh->Index_Count / 3);//Copia el VertexBuffer a la memoria. Me recomendaron que divida el Index buffer por 3 , asi que ahi esta
			meshCount++;
		}

		else
		{
			return;
		}
	}

	
}
