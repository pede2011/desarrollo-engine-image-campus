#pragma once
#include "Actor.h"
#include "Transformacion.h"
#include "Mesh.h"

class ENGINE_API Actor3D : public Actor
{
//Todo esto es para 3D
public:
	Actor3D();
	~Actor3D();
	Mesh* propMesh;
	Transformacion* transform;
	void Draw() override;
};