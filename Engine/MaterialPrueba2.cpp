#include "stdafx.h"
#include "MaterialPrueba2.h"
#include "NAPGame.h"


MaterialPrueba2::MaterialPrueba2(Textura* _text1, Textura* _text2)
{
	text1 = _text1;
	text2 = _text2;
}


MaterialPrueba2::~MaterialPrueba2()
{
}

void MaterialPrueba2::Set()
{
	NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

	NAPGame::gameInstance->d3ddev->SetTexture(0, text1->tex);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSU, text1->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSV, text1->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, text1->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, text1->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, text1->texFilter);

	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	
	NAPGame::gameInstance->d3ddev->SetTexture(1, text2->tex);
	NAPGame::gameInstance->d3ddev->SetSamplerState(1, D3DSAMP_ADDRESSU, text2->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(1, D3DSAMP_ADDRESSV, text2->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(1, D3DSAMP_MAGFILTER, text2->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(1, D3DSAMP_MINFILTER, text2->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(1, D3DSAMP_MIPFILTER, text2->texFilter);
	
	NAPGame::gameInstance->d3ddev->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_CURRENT);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 0);

	NAPGame::gameInstance->d3ddev->SetTextureStageState(2, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(3, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(4, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(5, D3DTSS_COLOROP, D3DTOP_DISABLE);
	NAPGame::gameInstance->d3ddev->SetTextureStageState(6, D3DTSS_COLOROP, D3DTOP_DISABLE);
}