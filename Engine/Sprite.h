#pragma once
#include "Material.h"
#include "Textura.h"

class ENGINE_API Sprite :public Material
{
public:
	Sprite(Textura* _textSprite);
	~Sprite();
	Textura* textSprite;
	void Set() override;
	float tileX=1;
	float tileY=1;
	float tileW=0;
	float tileH=0;
};
