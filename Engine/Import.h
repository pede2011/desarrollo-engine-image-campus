#pragma once

#include "GameObject.h"
#include "Mesh.h"
#include "MeshRenderer.h"
#include "NAPGame.h"
#include "Test.h"
#include "Textura.h"
#include "Material.h"
#include "MaterialAI.h"
#include <string>
#include <vector>

//ASSIMP
#include "../assimp/include/Importer.hpp"
#include "../assimp/include/scene.h"
#include "../assimp/include/postprocess.h"
#include "../assimp/include/matrix4x4.h"

//class aiScene;
//class aiNode;
//class aiMesh;
//class Mesh;

#pragma comment (lib, "../assimp/lib/assimp.lib")//Importo libreria ASSIMP

class ENGINE_API Import
{
public:
	Import();	
	~Import();	

	//const aiScene* scene;

	static Import* importerInstance;

	bool ConvertNodeToGO(Test* test, aiNode* node, const aiScene* scene, string direccion);

	Test* LoadScene(const char* pathFile);

	Mesh* LoadFileForDisplay(const char* pathFile);

	Mesh* DisplaySingleModel(aiMesh*);

	void getTransform4GO(Test* test, aiNode* node);

	aiTextureType Import::GetTextureType(aiMaterial* material);
};

