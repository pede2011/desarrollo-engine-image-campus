#pragma once
#include "Mesh.h"
#include "ActorCamara.h"

class ENGINE_API Frustum
{
public:
	Camara* cam;
	D3DXPLANE m_planes[6];

	Frustum();
	~Frustum();
	bool CheckFrustum(Mesh* _m);	
};

