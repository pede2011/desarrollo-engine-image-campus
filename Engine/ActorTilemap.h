#pragma once
#include "Tilemap.h"
#include "TileMapRenderer.h"

class ENGINE_API ActorTilemap:public Actor
{
public:
	ActorTilemap(TilemapRenderer* _tilemapRenderer);
	~ActorTilemap();
	TilemapRenderer* tilemapRenderer;
	void Draw() override;
	
};

