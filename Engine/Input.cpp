#include "stdafx.h"
#include "Input.h"
#include <map>
#include <iostream>
#include "NAPGame.h"

LPDIRECTINPUT8 Input::dinp = NULL;
LPDIRECTINPUTDEVICE8 Input::keyDev = NULL;
map<string, vector<int>> Input::keys;
byte Input::prevKeys[256];
byte Input::currentKeys[256];

Input::Input()
{
}


Input::~Input()
{
}


void Input::Initialize(HINSTANCE hInstance, HWND hWnd)
{

	///Inicializamos DirectInput
	DirectInput8Create(hInstance,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&dinp,
		NULL);

	dinp->CreateDevice(GUID_SysKeyboard, &keyDev, NULL);
	keyDev->SetDataFormat(&c_dfDIKeyboard);
	keyDev->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	keyDev->Acquire();

	for (int i = 0; i < 256; i++)
	{
		currentKeys[i] = 0;
	}
}

void Input::SetKey(std::string _keys, int _keyMapping)
{
	keys[_keys].push_back(_keyMapping);
}

void Input::Update()
{
	for (int i = 0; i < 256; i++)
	{
		prevKeys[i] = currentKeys[i];
	}
	keyDev->GetDeviceState(sizeof(currentKeys), &currentKeys);

}
bool Input::GetKey(string keyPress)
{
	for (auto Index = keys.begin(); Index != keys.end(); Index++)
	{
		if (Index->first == keyPress)
		{
			for (int i = 0; i < Index->second.size(); i++)
			{
				if (currentKeys[Index->second[i]])
					return true;
			}
		}
	}
	return false;
}

void Input::Release()
{
	keyDev->Unacquire();
	keyDev->Release();
	dinp->Release();
}