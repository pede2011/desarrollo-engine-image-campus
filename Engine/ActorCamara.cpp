#include "stdafx.h"
#include "ActorCamara.h"
#include "NAPGame.h"

ActorCamara::ActorCamara()
{
	//propCamara.trans = &propTransformacion;
}

ActorCamara::ActorCamara(Transformacion *transform, float fov) :propCamara(new Camara(transform, fov))
{
}

ActorCamara::~ActorCamara()
{
}

void ActorCamara::Update()
{
	Input::SetKey("Z", DIK_Z);

	if (NAPGame::gameInstance->input.GetKey("Z"))
	{
		propCamara->trans->position->z += 10.0f * NAPGame::gameInstance->deltaTime;
	}
}

void ActorCamara::Draw()
{
}