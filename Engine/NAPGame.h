#pragma once
#include "Camara.h"
#include <vector>
#include "Actor.h"
#include "Input.h"
#include "ActorCamara.h"

using namespace std;


class ENGINE_API NAPGame
{
private:
	//Variables privadas para la ventana
	int myWidth;
	int myHeight;
	HINSTANCE _hInst;
	int _nCmdShow;
	static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

public:
	NAPGame(HINSTANCE hInstance, int nCmdShow, int width, int height);
	
	void Run();//Funcion para mantener la ventana abierta
	virtual ~NAPGame();
	
	virtual void Update();//Aca van todas las modificaciones del estilo transformacion
	virtual void Draw();//Donde se dibuja la escena

	LPDIRECT3DDEVICE9 d3ddev = NULL;//Puntero a interfaz device DX. Aca esta en NULL
	LPDIRECT3D9 d3d = NULL; //Puntero a interfaz DX. Aca esta en NULL

	static NAPGame* gameInstance; // Singleton. Como costo esto, eh.
	vector<Actor*> vectorActor; //Vector vacio de actor

	//Delta Time
	long currentTime;
	float deltaTime;

	//ActorCamara* camara;

	HWND* hWnd;

	Input input;
};

