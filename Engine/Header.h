#pragma once

#ifdef ENGINE_EXPORTS
#define ENGINE_API __declspec(dllexport)
#else
#define ENGINE_API __declspec(dllimport)
#endif

#include <d3d9.h> //Busca el header de directx en los path
#include <d3dx9.h>//Agrega el otro header
#pragma comment (lib, "d3d9.lib") //Incluyo las lib a mi proyecto
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "d3dx9d.lib")
//#pragma comment (lib, "dinput8.lib")
//#pragma comment (lib, "dxguid.lib")

#define CUSTOMFVF2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)//Este es para 2D. Lo defino para no reescribir.
#define CUSTOMFVF3DMAT (D3DFVF_XYZ | D3DFVF_TEX1)//Este es para 3D con MATERIALES
#define CUSTOMFVF3DDIFF (D3DFVF_XYZ | D3DFVF_DIFFUSE)//Este es para 3D con COLORES
