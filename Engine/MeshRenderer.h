#pragma once
#include "Mesh.h" //Para obtener las propiedades
#include "Transformacion.h"
#include "Material.h"
#include "NAPGame.h"
#include "Textura.h"
#include "Frustum.h"

class ENGINE_API MeshRenderer
{
public:
	MeshRenderer(Transformacion* transformacion, Mesh* mesh, Material* material);
	~MeshRenderer();
	Mesh* propMesh;
	Transformacion* propTransform;
	//Textura* propTextura;
	Material* propMaterial;
	static Frustum* propFrustum;

	static int meshCount;

	//Carga el mesh pasandole un puntero a sus propiedades
	/*void loadMesh(Mesh *myMesh)
	{
		propMesh = myMesh;
	}*/
	/*void loadTrans(Transformacion *myTransformacion)
	{
		propTransform = myTransformacion;
	}*/

	//Dibuja el mesh igual que el Actor3D
	void DrawMesh();

	void LoadAnimMesh(Mesh* mesh);
};
