#pragma once
#include <vector>
#include "Mesh.h"
#include "Textura.h"
#include "Transformacion.h"

class ENGINE_API Tilemap
{
public:

	Tilemap(int _altoTileMap, int _anchoTileMap, int _cantCapas, std::vector<Mesh*>_mesh, Textura* _textTileMap);
	~Tilemap();

	int anchoTileMap;
	int altoTileMap;
	int cantCapas;

	float anchoTile;
	float altoTile;
	float tiling;

	Textura* textTileMap;
	std::vector<Mesh*> mesh;
	std::vector<std::vector<std::vector<byte>>> capa;	

	Transformacion* transform;

	void SetNumTile(int numCapa, int fila, int columna, int numTile);
	int GetIndexTile(int index, int posX, int posY);
};

