#pragma once
#include "Camara.h"
#include "Transformacion.h"
#include "Actor.h"

class ENGINE_API ActorCamara : Actor
{
public:
	ActorCamara();
	ActorCamara(Transformacion *transform, float fov);
	~ActorCamara();
	Camara *propCamara;
	void Update();
	void Draw();	
};

