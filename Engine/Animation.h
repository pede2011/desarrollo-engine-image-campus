#pragma once
#include "Mesh.h"
#include <vector>

class ENGINE_API Animation
{
public:
	Animation();
	~Animation();
	float velocidad = 0.2F;
	float animRate = 0.1f;
	std::vector<Mesh*>meshAnim;
	std::string nameAnim;
};

