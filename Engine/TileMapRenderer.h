#pragma once
#include "Tilemap.h"
#include "Material.h"
#include "Transformacion.h"
#include "NAPGame.h"
#include "MeshRenderer.h"

class ENGINE_API TilemapRenderer
{
public:

	TilemapRenderer(Tilemap* _TileMap);
	~TilemapRenderer();

	Tilemap* myTilemap;
	Transformacion* myTransformacion;
	Material myMaterial;
	MeshRenderer* myMeshRenderer;

	void DrawTilemap();
};

