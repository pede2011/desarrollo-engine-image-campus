#include "stdafx.h"
#include "Test.h"

Test::Test()
{
	myTransformacion = new Transformacion(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));
}

Test::Test(Mesh* mesh, Material* material)
{
	myTransformacion = new Transformacion(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));

	myMeshRenderer = new MeshRenderer(myTransformacion, mesh, material);

	//myAnimator.meshRenderer = myMeshRenderer;		
}


Test::~Test()
{
}

void Test::Draw()
{
	if (myMeshRenderer != NULL)
	{
		myMeshRenderer->DrawMesh();//Dibuja segun los componentes de MeshRenderer
	}
	for (int i = 0; i < sons.size(); i++)
	{
		sons[i]->Draw();
	}
}

void Test::Update()
{
	if (active==true)
	{
		/*if (Input::GetKey("F5"))
		{
			myAnimator.Play();
		}*/

		//Controles
		if (Input::GetKey("E"))
		{
			myTransformacion->rotation->z += NAPGame::gameInstance->deltaTime * 10000.0f;
		}
		if (Input::GetKey("Q"))
		{
			myTransformacion->rotation->z -= NAPGame::gameInstance->deltaTime * 10000.0f;
		}
		if (Input::GetKey("D"))
		{
			myTransformacion->position->x += NAPGame::gameInstance->deltaTime * 10000.0f;
		}

		if (Input::GetKey("W"))
		{
			myTransformacion->position->y += NAPGame::gameInstance->deltaTime * 10000.0f;
		}

		if (Input::GetKey("A"))
		{
			myTransformacion->position->x -= NAPGame::gameInstance->deltaTime * 10000.0f;
		}

		if (Input::GetKey("S"))
		{
			myTransformacion->position->y -= NAPGame::gameInstance->deltaTime * 10000.0f;
		}
		NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_WORLD, &myTransformacion->GetMatrix());
	}	
}

void Test::SetMesh(Mesh* m, Material* mat)
{
	myMeshRenderer = new MeshRenderer(myTransformacion, m, mat);
}

void Test::SetParent(Test* _father)
{
	if (_father == this)
		return;	
	father = _father;

	myTransformacion->SetParent(_father->myTransformacion);
	if (father == NULL)
	{
		father = _father;
		_father->sons.push_back(this);
	}
}

void Test::AddChild(Test* child)
{
	sons.push_back(child);
	myTransformacion->AddChild(child->myTransformacion);

}

void Test::SetTransform(aiMatrix4x4 m) 
{
	myTransformacion->ogTransform._11 = m.a1;
	myTransformacion->ogTransform._12 = m.a2;
	myTransformacion->ogTransform._13 = m.a3;
	myTransformacion->ogTransform._14 = m.a4;

	myTransformacion->ogTransform._21 = m.b1;
	myTransformacion->ogTransform._22 = m.b2;
	myTransformacion->ogTransform._23 = m.b3;
	myTransformacion->ogTransform._24 = m.b4;

	myTransformacion->ogTransform._31 = m.c1;
	myTransformacion->ogTransform._32 = m.c2;
	myTransformacion->ogTransform._33 = m.c3;
	myTransformacion->ogTransform._34 = m.c4;

	myTransformacion->ogTransform._41 = m.d1;
	myTransformacion->ogTransform._42 = m.d2;
	myTransformacion->ogTransform._43 = m.d3;
	myTransformacion->ogTransform._44 = m.d4;
}
