#pragma once
#include "Material.h"
#include "Textura.h"

class ENGINE_API MaterialAI :public Material
{
public:
	MaterialAI(Textura* _text1);
	~MaterialAI();
	Textura* text1;
	void Set() override;
};
