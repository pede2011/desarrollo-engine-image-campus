#include "stdafx.h"
#include "Animator.h"
#include "Sprite.h"
#include "NAPGame.h"

Animator::Animator()
{
}

Animator::~Animator()
{
}

void Animator::Play()
{
	num += NAPGame::gameInstance->deltaTime;

	while (num>animation->animRate)
	{
		indice++;
		num -= animation->animRate;
		if (indice>=animation->meshAnim.size())
		{
			indice = 0;
		}
		meshRenderer->LoadAnimMesh(animation->meshAnim[indice]);
	}
}

void Animator::setAnimation(string _nameAnim)
{
	for (int i = 0; i < animArray.size(); i++)
	{
		if (animArray[i]->nameAnim==_nameAnim)
		{
			animation = animArray[i];
		}
	}
}