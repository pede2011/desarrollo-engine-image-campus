#pragma once

class ENGINE_API Material
{
public:
	Material();
	~Material();
	virtual void Set();
};
