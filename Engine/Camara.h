#pragma once
#include "Header.h"
#include <vector>

class Transformacion;
class ENGINE_API Camara
{
public:
	float fov;
	int ZNear = 1;
	int ZFar = 3000;
	Camara();
	Camara(Transformacion *trans, float fov);
	~Camara();
	void UpdateCamara();	
	//D3DXMATRIX matView;
	D3DXVECTOR3 camLookPos;
	D3DXVECTOR3 camUp;
	Transformacion* trans;
	static std::vector < Camara* > camara;
};