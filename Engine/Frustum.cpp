#include "stdafx.h"
#include "Frustum.h"


Frustum::Frustum()
{
//	cam = NAPGame::gameInstance->camara->propCamara;

	float zMinimum;
	//float screenDepth = cam->ZFar;
	float screenDepth = 3000;
	float r;
	
	D3DXMATRIX matrix;
	D3DXMATRIX projectionMatrix;
	NAPGame::gameInstance->d3ddev->GetTransform(D3DTS_PROJECTION, &projectionMatrix);
	D3DXMATRIX viewMatrix;
	NAPGame::gameInstance->d3ddev->GetTransform(D3DTS_VIEW, &viewMatrix);

	// Calculate the minimum Z distance in the frustum.
	zMinimum = -projectionMatrix._43 / projectionMatrix._33;
	r = screenDepth / (screenDepth - zMinimum);
	projectionMatrix._33 = r;
	projectionMatrix._43 = -r * zMinimum;

	// Create the frustum matrix from the view matrix and updated projection matrix.
	D3DXMatrixMultiply(&matrix, &viewMatrix, &projectionMatrix);

	// Calculate near plane of frustum.
	m_planes[0].a = matrix._14 + matrix._13;
	m_planes[0].b = matrix._24 + matrix._23;
	m_planes[0].c = matrix._34 + matrix._33;
	m_planes[0].d = matrix._44 + matrix._43;

	// Calculate far plane of frustum.
	m_planes[1].a = matrix._14 - matrix._13;
	m_planes[1].b = matrix._24 - matrix._23;
	m_planes[1].c = matrix._34 - matrix._33;
	m_planes[1].d = matrix._44 - matrix._43;

	// Calculate left plane of frustum.
	m_planes[2].a = matrix._14 + matrix._11;
	m_planes[2].b = matrix._24 + matrix._21;
	m_planes[2].c = matrix._34 + matrix._31;
	m_planes[2].d = matrix._44 + matrix._41;

	// Calculate right plane of frustum.
	m_planes[3].a = matrix._14 - matrix._11;
	m_planes[3].b = matrix._24 - matrix._21;
	m_planes[3].c = matrix._34 - matrix._31;
	m_planes[3].d = matrix._44 - matrix._41;

	// Calculate top plane of frustum.
	m_planes[4].a = matrix._14 - matrix._12;
	m_planes[4].b = matrix._24 - matrix._22;
	m_planes[4].c = matrix._34 - matrix._32;
	m_planes[4].d = matrix._44 - matrix._42;

	// Calculate bottom plane of frustum.
	m_planes[5].a = matrix._14 + matrix._12;
	m_planes[5].b = matrix._24 + matrix._22;
	m_planes[5].c = matrix._34 + matrix._32;
	m_planes[5].d = matrix._44 + matrix._42;

	for (size_t i = 0; i < 6; i++)
	{
		D3DXPlaneNormalize(&m_planes[i], &m_planes[i]);
	}
}

Frustum::~Frustum()
{
}

bool Frustum::CheckFrustum(Mesh* _m)
{
	Mesh* mesh = _m;	

	D3DXVECTOR3 minBounds, maxBounds;

	DWORD* fvf;

	D3DXComputeBoundingBox((D3DXVECTOR3*)mesh->vb, mesh->Vertex_Count, D3DXGetFVFVertexSize(CUSTOMFVF3DMAT), &minBounds, &maxBounds);//El FVF hardcodeado porque fiaca modificar todo el NAPGame

	D3DXVECTOR3 m_objectBounds[8];

	m_objectBounds[0] = D3DXVECTOR3(minBounds.x, minBounds.y, minBounds.z); // xyz
	m_objectBounds[1] = D3DXVECTOR3(maxBounds.x, minBounds.y, minBounds.z); // Xyz
	m_objectBounds[2] = D3DXVECTOR3(minBounds.x, maxBounds.y, minBounds.z); // xYz
	m_objectBounds[3] = D3DXVECTOR3(maxBounds.x, maxBounds.y, minBounds.z); // XYz
	m_objectBounds[4] = D3DXVECTOR3(minBounds.x, minBounds.y, maxBounds.z); // xyZ
	m_objectBounds[5] = D3DXVECTOR3(maxBounds.x, minBounds.y, maxBounds.z); // XyZ
	m_objectBounds[6] = D3DXVECTOR3(minBounds.x, maxBounds.y, maxBounds.z); // xYZ
	m_objectBounds[7] = D3DXVECTOR3(maxBounds.x, maxBounds.y, maxBounds.z); // XYZ



	for (int i = 0; i<6; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (D3DXPlaneDotCoord(&m_planes[i], &m_objectBounds[j]) >= 0.0f)
			{
				return false;//If the OBJ is at anytime out of bounds, it wont render
			}

			else
			{
				return true;
			}
		}
	}
}
