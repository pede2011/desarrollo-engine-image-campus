#pragma once
#include "NAPGame.h"
#include "Transformacion.h"

class ENGINE_API Transformacion
{
public:
	Transformacion();
	Transformacion(D3DXVECTOR3 posicion, D3DXVECTOR3 rotacion, D3DXVECTOR3 escala);
	~Transformacion();

	D3DXVECTOR3 *position;
	D3DXVECTOR3 *rotation;
	D3DXVECTOR3 *scale;
	D3DXMATRIX ogTransform;

	//SceneGraph
	std::vector<Transformacion*> *sons = new std::vector<Transformacion*>;
	Transformacion *Parent = NULL;

	void SetParent(Transformacion *_parent);
	void AddChild(Transformacion *child);
	bool Remove(Transformacion* _transform);

	D3DXMATRIX GetMatrix();
};