#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "stdafx.h"
#include "Import.h"

//Essentials
#include <limits>
#include <cstddef>
#include <iostream>
#include <stdint.h>
#include <list>
#include <assert.h>
#include <string>

using namespace std;

Import* Import::importerInstance = NULL;

Import::Import()
{
	importerInstance = this;
}

Import::~Import()
{

}

Mesh* Import::LoadFileForDisplay(const char* pathFile)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(pathFile,
		aiPrimitiveType_LINE|
		aiPrimitiveType_POINT|
		aiProcess_Triangulate|
		aiProcess_SortByPType|
		aiProcess_MakeLeftHanded|
		aiProcess_FlipWindingOrder|
		aiProcess_GenUVCoords);

	if (scene->mMeshes[0])
	{
		return this->DisplaySingleModel(scene->mMeshes[0]);
	}

	else
	{
		return NULL;
	}
}



Test* Import::LoadScene(const char* pathFile)
{	
	std::string str(pathFile);

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(pathFile,
		aiPrimitiveType_LINE|
		aiPrimitiveType_POINT|
		aiProcess_Triangulate|
		aiProcess_SortByPType|
		aiProcess_MakeLeftHanded|
		aiProcess_GenSmoothNormals|
		aiProcess_FlipUVs);
		
	if (!scene)
		return NULL;

	Test* test = new Test();
	if (scene->mRootNode != NULL)
	{
		ConvertNodeToGO(test, scene->mRootNode, scene, str);
	}
	return test;
}


bool Import::ConvertNodeToGO(Test* test, aiNode* node, const aiScene* scene, string direccion)
{
	getTransform4GO(test, node);
	test->name = node->mName.C_Str();
	// Seteo padres e hijos
	for (size_t i = 0; i < node->mNumChildren; i++)
	{
		Test *child = new Test();
		test->sons.push_back(child);		
		child->SetParent(test);
		ConvertNodeToGO(child, node->mChildren[i], scene, direccion);
	}

	//Seteo el hijo y su mesh
	for (size_t i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* currentMesh = scene->mMeshes[node->mMeshes[i]];
		Test* child = new Test();
		test->meshName = "Drawing mesh";

		aiMaterial* meshMat = scene->mMaterials[scene->mMeshes[node->mMeshes[i]]->mMaterialIndex];

		size_t found = direccion.find_last_of("/\\");
		string texPath = direccion.substr(0, found + 1);
		aiString path;
		meshMat->Get(AI_MATKEY_TEXTURE(GetTextureType(meshMat), 0), path);
		texPath.append(path.C_Str());

		std::string stemp = std::string(texPath.begin(), texPath.end());
		Textura* auxTex = new Textura(stemp);
		
		//string temp = "white.jpg";
		//Textura* auxTex = new Textura(temp);
		
		Material* mat = new MaterialAI(auxTex);

		child->SetMesh(DisplaySingleModel(currentMesh), mat);
		test->AddChild(child);
		child->SetParent(test);		
	}
	return true;
}

void Import::getTransform4GO(Test* test, aiNode* node)
{
	aiMatrix4x4 m = node->mTransformation.Transpose();
	test->SetTransform(m);
}

aiTextureType Import::GetTextureType(aiMaterial* _material)
{
	aiTextureType type = aiTextureType_DIFFUSE;
	aiMaterial* material = _material;

	if (material->GetTextureCount(aiTextureType_AMBIENT) != 0)
	{
		type = aiTextureType_AMBIENT;
	}
	if (material->GetTextureCount(aiTextureType_DIFFUSE) != 0)
	{
		type = aiTextureType_DIFFUSE;
	}
	if (material->GetTextureCount(aiTextureType_DISPLACEMENT) != 0)
	{
		type = aiTextureType_DISPLACEMENT;
	}
	if (material->GetTextureCount(aiTextureType_EMISSIVE) != 0)
	{
		type = aiTextureType_EMISSIVE;
	}
	if (material->GetTextureCount(aiTextureType_HEIGHT) != 0)
	{
		type = aiTextureType_HEIGHT;
	}
	if (material->GetTextureCount(aiTextureType_LIGHTMAP) != 0)
	{
		type = aiTextureType_LIGHTMAP;
	}
	if (material->GetTextureCount(aiTextureType_NONE) != 0)
	{
		type = aiTextureType_NONE;
	}
	if (material->GetTextureCount(aiTextureType_NORMALS) != 0)
	{
		type = aiTextureType_NORMALS;
	}
	if (material->GetTextureCount(aiTextureType_OPACITY) != 0)
	{
		type = aiTextureType_OPACITY;
	}
	if (material->GetTextureCount(aiTextureType_REFLECTION) != 0)
	{
		type = aiTextureType_REFLECTION;
	}
	if (material->GetTextureCount(aiTextureType_SHININESS) != 0)
	{
		type = aiTextureType_SHININESS;
	}
	if (material->GetTextureCount(aiTextureType_SPECULAR) != 0)
	{
		type = aiTextureType_SPECULAR;
	}
	if (material->GetTextureCount(aiTextureType_UNKNOWN) != 0)
	{
		type = aiTextureType_UNKNOWN;
	}
	return type;
}

Mesh* Import::DisplaySingleModel(aiMesh* currentMesh)
{
	Vertex *AICastVertex = new Vertex[currentMesh->mNumVertices];
	int AICastVertexCount = currentMesh->mNumVertices;
	int indexSize = currentMesh->mNumFaces * 3;
	WORD *AICastIndex = new unsigned short[indexSize];

	for (int j = 0; j < currentMesh->mNumVertices; j++)
	{
		//XYZ
		AICastVertex[j].x = currentMesh->mVertices[j].x;
		AICastVertex[j].y = currentMesh->mVertices[j].y;
		AICastVertex[j].z = currentMesh->mVertices[j].z;
		//UV
		const aiVector3D Cero3D(0.0f, 0.0f, 0.0f);
		const aiVector3D* pTexCoord = currentMesh->HasTextureCoords(0) ? &(currentMesh->mTextureCoords[0][j]) : &Cero3D;

		AICastVertex[j].u = pTexCoord->x;
		AICastVertex[j].v = pTexCoord->y;
			//AICastVertex[j].u = currentMesh->mTextureCoords[0][j].x;
			//AICastVertex[j].v = currentMesh->mTextureCoords[0][j].y;
	}
	//Index	
	for (size_t i = 0; i < currentMesh->mNumFaces; i++) 
	{
		assert(currentMesh->mFaces[i].mNumIndices == 3);

		AICastIndex[i * 3 + 0] = currentMesh->mFaces[i].mIndices[0];
		AICastIndex[i * 3 + 1] = currentMesh->mFaces[i].mIndices[1];
		AICastIndex[i * 3 + 2] = currentMesh->mFaces[i].mIndices[2];
	}

	Mesh* m = new Mesh(AICastVertex, AICastIndex, AICastVertexCount, AICastVertexCount * 3);
	return m;
}