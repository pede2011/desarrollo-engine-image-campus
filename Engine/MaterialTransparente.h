#pragma once
#include "Material.h"
#include "Textura.h"

class ENGINE_API MaterialTransparente :public Material
{
public:
	MaterialTransparente(Textura* _text1);
	~MaterialTransparente();
	Textura* text1;
	void Set() override;
};

