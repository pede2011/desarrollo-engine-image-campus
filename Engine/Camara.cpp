#include "stdafx.h"
#include "NAPGame.h"
#include "Camara.h"
#include "Transformacion.h"

vector<Camara*> Camara::camara;

Camara::Camara() :fov(90.0f), trans(NULL)
{
	Camara::camara.push_back (this);
}

Camara::Camara(Transformacion *trans, float fov) : trans(trans), fov(fov)
{
	Camara::camara.push_back(this);
}

Camara::~Camara()
{
}

void Camara::UpdateCamara()
{
	if (trans!=NULL)
	{
		D3DXVECTOR3 forward(0.0f, 0.0f, 1.0f);
		D3DXVECTOR3 up(0.0f, 0.0f, 1.0f);

		D3DXMATRIX matView;		
		D3DXMatrixLookAtLH(&matView, &(*trans->position - D3DXVECTOR3(5.0f, 5.0f, 5.0f)), &(*trans->position+forward), &up);

		D3DXMATRIX projView;
		D3DXMatrixPerspectiveFovLH(&projView, D3DXToRadian(fov), (float)800 / (float)600, ZNear, ZFar);

		NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_VIEW, &matView);
		NAPGame::gameInstance->d3ddev->SetTransform(D3DTS_PROJECTION, &projView);
	}
	
}