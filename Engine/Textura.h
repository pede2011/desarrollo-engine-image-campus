#pragma once
#include <string>
using namespace std;

class ENGINE_API Textura
{
public:
	
	LPDIRECT3DTEXTURE9 tex = NULL;
	D3DTEXTUREFILTERTYPE texFilter = D3DTEXF_ANISOTROPIC;
	D3DTEXTUREADDRESS texAd = D3DTADDRESS_MIRROR;
	
	Textura(LPCWSTR path);
	Textura(const string&  _texture);
	~Textura();

	void SetTexture(int channel);
};
