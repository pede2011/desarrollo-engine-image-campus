#include "stdafx.h"
#include "NAPGame.h"
#include "Textura.h"

Textura::Textura(LPCWSTR path)
{
	D3DXCreateTextureFromFile(NAPGame::gameInstance->d3ddev, path, &tex);
}

Textura::Textura(const std::string&  _texture)
{
	//Transforma un string a LPCWSTR
	//-----------------------------------
	int len;
	int slength = (int)_texture.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, _texture.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, _texture.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;

	LPCWSTR result = r.c_str();
	//-----------------------------------

	//D3DXCreateTextureFromFile(NAPGame::gameInstance->d3ddev, result, &tex);

	HRESULT HT = D3DXCreateTextureFromFileEx(NAPGame::gameInstance->d3ddev,
								result, 0, 0, 0, 0,
								D3DFMT_UNKNOWN, D3DPOOL_MANAGED, 
								D3DX_FILTER_NONE, D3DX_FILTER_NONE, 
								D3DCOLOR_XRGB(NULL, NULL, NULL),
								NULL, NULL, &tex);

}

Textura::~Textura()
{

}

void Textura::SetTexture(int channel)
{
	NAPGame::gameInstance->d3ddev->SetTexture(channel, tex);

	//Comentado por ASSIMP, deber�a funcionar igual
	/*NAPGame::gameInstance->d3ddev->SetSamplerState(channel, D3DSAMP_ADDRESSU, this->texAd);
	NAPGame::gameInstance->d3ddev->SetSamplerState(channel, D3DSAMP_ADDRESSV, this->texAd);

	NAPGame::gameInstance->d3ddev->SetSamplerState(channel, D3DSAMP_MAGFILTER, this->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(channel, D3DSAMP_MINFILTER, this->texFilter);
	NAPGame::gameInstance->d3ddev->SetSamplerState(channel, D3DSAMP_MIPFILTER, this->texFilter);*/
}
