#pragma once
#include "Material.h"
#include "Textura.h"

class ENGINE_API MaterialPrueba1:public Material
{
public:
	MaterialPrueba1(Textura* _text1, Textura* _text2);
	~MaterialPrueba1();
	Textura* text1;
	Textura* text2;
	void Set() override;
};

