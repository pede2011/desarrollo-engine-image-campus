#include "stdafx.h"
#include "ActorTilemap.h"


ActorTilemap::ActorTilemap(TilemapRenderer* _tilemapRenderer)
{
	tilemapRenderer = _tilemapRenderer;
}


ActorTilemap::~ActorTilemap()
{
}

void ActorTilemap::Draw()
{
	tilemapRenderer->DrawTilemap();
}

