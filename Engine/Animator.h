#pragma once
#include "Mesh.h"
#include "Animation.h"
#include "Sprite.h"
#include "MeshRenderer.h"

class ENGINE_API Animator
{
public:
	Animator ();
	~Animator();
	Animation* animation;
	Sprite* sprite;
	MeshRenderer* meshRenderer;
	std::vector<Animation*>animArray;
	void Play();
	void setAnimation(std::string _nameAnim);
	float num = 0;
	int indice = 0;
};

