#pragma once
#include "dinput.h"
#include <map>
#include <vector>
#pragma comment (lib, "dinput8.lib") //Incluyo la lib a mi proyecto
#pragma comment (lib, "dxguid.lib")

using namespace std;

class ENGINE_API Input
{
public:
	Input();
	~Input();

	static LPDIRECTINPUT8 dinp;
	static LPDIRECTINPUTDEVICE8 keyDev;
	static map<string, vector<int>> keys;
	static byte prevKeys[256];
	static byte currentKeys[256];

	static void Initialize(HINSTANCE hInstance, HWND hWnd);
	static void SetKey(string keys, int keyMapping);
	static bool GetKey(string keyPress);
	static void Update();
	static void Release();
};

