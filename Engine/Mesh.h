#pragma once

struct Vertex//Predefino lo necesario para un render
{
	float x, y, z, u, v;//por ahora es 2D
};
struct ColorVertex
{
	FLOAT x, y, z;
	DWORD color;
};

class ENGINE_API Mesh
{
public:
	Mesh(ColorVertex vertexes[], WORD indexes[], int p_Vertex_Count, int p_Index_Count);
	Mesh(Vertex vertexes[], WORD indexes[], int p_Vertex_Count, int p_Index_Count);
	~Mesh();

	//Genero ints para no tener que hardcodear valores
	int Index_Count;
	int Vertex_Count;

	//Punteros a interfaces de DX
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
};