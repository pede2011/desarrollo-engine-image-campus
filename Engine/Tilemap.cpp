#include "stdafx.h"
#include "Tilemap.h"

Tilemap::Tilemap(int _altoTileMap, int _anchoTileMap, int _cantCapas, std::vector<Mesh*>_mesh, Textura* _textTileMap)
{
	altoTileMap = _altoTileMap;
	anchoTileMap = _anchoTileMap;
	cantCapas = _cantCapas;
	mesh = _mesh;
	textTileMap = _textTileMap;

	capa.resize(cantCapas);
	for (int i = 0; i < cantCapas; i++)
	{
		capa[i].resize(altoTileMap);
		for (int j = 0; j < altoTileMap; j++)
		{
			capa[i][j].resize(anchoTileMap);
		}
	}
}


Tilemap::~Tilemap()
{
}


void Tilemap::SetNumTile(int indiceCapa, int fila, int columna, int indiceTile)
{
	capa[indiceCapa][fila][columna] = indiceTile;
}

int Tilemap::GetIndexTile(int indiceCapa, int posX, int posY)
{
	if (0 <= indiceCapa && 0 <= posY && 0 <= posX)
	{
		if (cantCapas > indiceCapa && altoTileMap > posY && anchoTileMap > posX)
		{
			return capa[indiceCapa][posY][posX];
		}
		return 0;
	}
}