#include "stdafx.h"
#include "Transformacion.h"
//-Crear componente Transform con posici�n, rotaci�n y escala (vector3).

Transformacion::Transformacion()
{
}

Transformacion::Transformacion(D3DXVECTOR3 posicion, D3DXVECTOR3 rotacion, D3DXVECTOR3 escala)
{
	position = new D3DXVECTOR3(posicion);
	rotation = new D3DXVECTOR3(rotacion);
	scale = new D3DXVECTOR3(escala);
}

Transformacion::~Transformacion()
{
}

void Transformacion::SetParent(Transformacion* _parent) 
{
	if (_parent == this)		
		return;		
	Parent = _parent;

	if (Parent == NULL)
	{
		Parent = _parent;
		_parent->sons->push_back(this);
	}

}

void Transformacion::AddChild(Transformacion* child)
{
	sons->push_back(child);
}

bool Transformacion::Remove(Transformacion* _transform)
{
	for (std::vector<Transformacion*>::iterator iterator = sons->begin(); iterator != sons->end(); iterator++)
	{
		if (*iterator == _transform)
		{
			sons->erase(iterator);
			return true;
		}
	}
	return false;
}

D3DXMATRIX Transformacion::GetMatrix()
{
	D3DXMATRIX matTrans;
	D3DXMATRIX matSca;
	D3DXMATRIX matRotX;
	D3DXMATRIX matRotY;
	D3DXMATRIX matRotZ;
	D3DXMatrixTranslation(&matTrans, position->x, position->y, position->z);
	D3DXMatrixScaling(&matSca, scale->x, scale->y, scale->z);
	D3DXMatrixRotationX(&matRotX, D3DXToRadian(rotation->x));
	D3DXMatrixRotationY(&matRotY, D3DXToRadian(rotation->y));
	D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(rotation->z));

	if (Parent == NULL)
	{
		D3DXMATRIX mat;
		D3DXMatrixIdentity(&mat);
		D3DXMatrixMultiply(&mat, &ogTransform, &mat);
		D3DXMatrixMultiply(&mat, &matTrans, &mat);
		D3DXMatrixMultiply(&mat, &matRotX, &mat);
		D3DXMatrixMultiply(&mat, &matRotY, &mat);
		D3DXMatrixMultiply(&mat, &matRotZ, &mat);
		D3DXMatrixMultiply(&mat, &matSca, &mat);

		return mat;
	}
	else
	{
		D3DXMATRIX mat;
		D3DXMatrixIdentity(&mat);
		D3DXMatrixMultiply(&mat, &Parent->ogTransform, &mat);

		D3DXMatrixMultiply(&mat, &matTrans, &mat);
		D3DXMatrixMultiply(&mat, &matRotX, &mat);
		D3DXMatrixMultiply(&mat, &matRotY, &mat);
		D3DXMatrixMultiply(&mat, &matRotZ, &mat);
		D3DXMatrixMultiply(&mat, &matSca, &mat);

		D3DXMatrixMultiply(&mat, &mat, &Parent->GetMatrix());

		return mat;
	}


	for (std::vector<Transformacion*>::iterator i = sons->begin(); i != sons->end(); i++)
	{
		(*i)->GetMatrix();
	}
}
