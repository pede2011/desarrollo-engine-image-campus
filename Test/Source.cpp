#include "stdafx.h"
#include "MyGame.h"
#include "Actor.h"

int WINAPI WinMain
(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR    lpCmdLine,
	int       nCmdShow
)

{
	MyGame main(hInstance, nCmdShow, 800, 600);

	main.Run();

	return 0;
}
