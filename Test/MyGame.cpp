#include "stdafx.h"
#include "MyGame.h"
#include "Test.h"
#include "Textura.h"
#include "ActorCamara.h"
#include "MaterialPrueba2.h"
#include "MaterialPrueba1.h"
#include "MaterialTransparente.h"
#include "MaterialAI.h"
#include "Sprite.h"
#include "Tilemap.h"
#include "Input.h"
#include "TileMapRenderer.h"
#include "ActorTilemap.h"
#include "Import.h"
#include "Frustum.h"
#include <chrono>

//#include "Actor3D.h"

using namespace std::chrono;

MyGame::MyGame(HINSTANCE hInstance, int nCmdShow, int width, int height) : NAPGame(hInstance, nCmdShow, width, height)
{
	Input::SetKey("F5", DIK_F5);

	Input::SetKey("W", DIK_W);
	Input::SetKey("W", DIK_UP);
	Input::SetKey("D", DIK_D);
	Input::SetKey("D", DIK_RIGHT);
	Input::SetKey("A", DIK_A);
	Input::SetKey("A", DIK_LEFT);
	Input::SetKey("S", DIK_S);
	Input::SetKey("S", DIK_DOWN);
	Input::SetKey("Q", DIK_Q);
	Input::SetKey("E", DIK_E);

	Textura* testTex1 = new Textura(L"koala.jpg");
	Textura* testTex2 = new Textura(L"faro.jpg");
	Textura* testTex3 = new Textura(L"datboi.jpg");
	Textura* testTex4 = new Textura(L"menem.jpg");
	Textura* testTex5 = new Textura(L"comandante.png");
	Textura* animTex = new Textura(L"sprites.png");
	Textura* tilemapTex = new Textura(L"tilemap.png");


	//Animation* animation = new Animation();

	//Adicion
	Material* mat1 = new MaterialPrueba1(testTex4, testTex3);
	//Multiplicacion
	Material* mat2 = new MaterialPrueba2(testTex1, testTex2);
	//Transparencia
	Material* mat3 = new MaterialTransparente(testTex5);
	Material* testSprite = new Sprite(animTex);
	
	vector<Mesh*> mesh;
	
/*Vertex vertexes[] =//Seteo los vertices del cuadrado y sus colores
{
	{ -0.5f, 0.5f, -0.5f, 0.0f, 0.0f}, // 0 
	{ 0.5f, 0.5f, -0.5f, 0.0f, 0.0f}, // 1 
	{ 0.5f, 0.5f, 0.5f, 0.0f, 0.0f}, // 2 
	{ -0.5f, 0.5f, 0.5f, 0.0f, 0.0f}, // 3

	{ -0.5f, -0.5f, 0.5f, 0.0f, 0.0f}, // 4
	{ 0.5f, -0.5f, 0.5f, 0.0f, 0.0f}, // 5
	{ 0.5f, -0.5f, -0.5f, 0.0f, 0.0f}, // 6
	{ -0.5f, -0.5f, -0.5f, 0.0f, 0.0f} // 7
};
	
WORD indexes[]  = 
		{ 0, 1, 2, 0, 2, 3,
		4, 5, 6, 4, 6, 7,
		3, 2, 5, 3, 5, 4,
		2, 1, 6, 2, 6, 5,
		1, 7, 6, 1, 0, 7,
		0, 3, 4, 0, 4, 7};

int Vertexes_Count = 8;
int Indexes_Count = 36;

	Mesh* m = new Mesh(vertexes, indexes, Vertexes_Count, Indexes_Count);	*/	

	NAPGame::gameInstance->d3ddev->SetFVF(CUSTOMFVF3DMAT);

	Material* matTest = new MaterialAI(testTex1);
	//Test* pruebaAI = new Test(Import::importerInstance->LoadFileForDisplay("../models/tetera/tetera.obj"), matTest);


	//--------------------------------------------TANK----------------------------------------------------
	//Transformacion *t = new Transformacion(D3DXVECTOR3(-40.0f, -10.0f, -50.0f), D3DXVECTOR3(33.0f, -90.0f, 112.0f), D3DXVECTOR3(10.0f, 10.0f, 10.0f));
	//ActorCamara* ac = new ActorCamara(t, 60);
	//Test*prueba = Import::importerInstance->LoadScene("../models/tank/tank.x");//new Tank(mesh);
	//NAPGame::gameInstance->d3ddev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);//Poner wireframe para ver el tanque mas facil. Gracias Gaby!


	//--------------------------------------------TINY--------------------------------------------------
	Transformacion *t = new Transformacion(D3DXVECTOR3(-80.0f, -150.0f, -200.0f), D3DXVECTOR3(33.0f, -90.0f, 112.0f), D3DXVECTOR3(10.0f, 10.0f, 10.0f));
	//Transformacion *t = new Transformacion(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	ActorCamara* ac = new ActorCamara(t, 60);
	//Test* c = new Test(m, matTest);
	//NAPGame::gameInstance->camara = ac;

	Test*prueba = Import::importerInstance->LoadScene("../models/tiny/tiny.x");//new Tiny(mesh);

	prueba->active = true;
}

MyGame::~MyGame()
{
}
